#!/usr/bin/python
# -*- coding: utf-8 -*-

# This module is proudly sponsored by CGI (www.cgi.com) and
# KPN (www.kpn.com).
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function
__metaclass__ = type


ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['preview'],
                    'supported_by': 'community'}

DOCUMENTATION = '''
---
module: icinga2_hostgroup
short_description: Manage a hostgroup in Icinga2
description:
   - "Add or remove a hostgroup to Icinga2 through the API."
   - "See U(https://www.icinga.com/docs/icinga2/latest/doc/12-icinga2-api/)"
version_added: "2.5"
author: "Jurgen Brand (@t794104)"
options:
  url:
    description:
      - HTTP, HTTPS, or FTP URL in the form (http|https|ftp)://[user[:pass]]@host.domain[:port]/path
    required: true
  use_proxy:
    description:
      - If C(no), it will not use a proxy, even if one is defined in
        an environment variable on the target hosts.
    type: bool
    default: 'yes'
  validate_certs:
    description:
      - If C(no), SSL certificates will not be validated. This should only be used
        on personally controlled sites using self-signed certificates.
    type: bool
    default: 'yes'
  url_username:
    description:
      - The username for use in HTTP basic authentication.
      - This parameter can be used without C(url_password) for sites that allow empty passwords.
  url_password:
    description:
        - The password for use in HTTP basic authentication.
        - If the C(url_username) parameter is not specified, the C(url_password) parameter will not be used.
  force_basic_auth:
    description:
      - httplib2, the library used by the uri module only sends authentication information when a webservice
        responds to an initial request with a 401 status. Since some basic auth services do not properly
        send a 401, logins will fail. This option forces the sending of the Basic authentication header
        upon initial request.
    type: bool
    default: 'no'
  client_cert:
    description:
      - PEM formatted certificate chain file to be used for SSL client
        authentication. This file can also include the key as well, and if
        the key is included, C(client_key) is not required.
  client_key:
    description:
      - PEM formatted file that contains your private key to be used for SSL
        client authentication. If C(client_cert) contains both the certificate
        and key, this option is not required.
  state:
    description:
      - Apply feature state.
    choices: [ "present", "absent" ]
    default: present
  hostgroupname:
    description:
      - Name used to create / delete the hostgroup on the host: "hostgroupname". This does not need to be the FQDN, but does needs to be unique.
    required: true
  zone:
    description:
      - The zone from where this host should be polled.
  template:
    description:
      - The template used to define the hostgroup.
      - Template cannot be modified after object creation.
  check_command:
    description:
      - The command used to call the hostgroup.
    default: "passive"
  display_name:
    description:
      - The name used to display the hostgroup.
    default: if none is give it is the value of the <hostgroupname> parameter
  ip:
    description:
      - The IP address of the host.
    required: true
  variables:
    description:
      - List of variables.
'''

EXAMPLES = '''
- hostgroupname: Add hostgroup to host in Icinga2
  icinga2_hostgroup:
    url: "https://icinga2.example.com"
    url_username: "ansible"
    url_password: "a_secret"
    state: present
    hostgroupname: "{{ ansible_fqdn }}"
    #ip: "{{ ansible_default_ipv4.address }}"
  delegate_to: 127.0.0.1
'''

RETURN = '''
hostgroupname:
    description: The name of host for which a hostgroup is created, modified or deleted
    type: string
    returned: always
data:
    description: The data structure used for create, modify or delete of the host
    type: dict
    returned: always
'''

import json
import os

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import fetch_url, url_argument_spec


# ===========================================
# Icinga2 API class
#
class icinga2_api:
    module = None

    def __init__(self, module):
        self.module = module

    def call_url(self, path, data='', method='GET'):
        headers = {
            'Accept': 'application/json',
            'X-HTTP-Method-Override': method,
        }
        url = self.module.params.get("url") + "/" + path
        rsp, info = fetch_url(module=self.module, url=url, data=data, headers=headers, method=method, use_proxy=self.module.params['use_proxy'])
        body = ''
        if rsp:
            body = json.loads(rsp.read())
        if info['status'] >= 400:
            body = info['body']
        return {'code': info['status'], 'data': body}

    def check_connection(self):
        ret = self.call_url('v1/status')
        if ret['code'] == 200:
            return True
        return False

    def exists(self, hostgroupname):
        data = {
            "filter": "match(\"" + hostgroupname+ "\", hostgroup.__name)",
        }
        ret = self.call_url(
            path="v1/objects/hostgroups",
            data=self.module.jsonify(data)
        )
        if ret['code'] == 200:
            if len(ret['data']['results']) == 1:
                return True
        return False

    def create(self, hostgroupname, data):
        ret = self.call_url(
            path="v1/objects/hostgroups/" + hostgroupname,
            data=self.module.jsonify(data),
            method="PUT"
        )
        return ret

    def delete(self, hostgroupname):
        data = {"cascade": 1}
        ret = self.call_url(
            path="v1/objects/hostgroups/" + hostgroupname,
            data=self.module.jsonify(data),
            method="DELETE"
        )
        return ret

    def modify(self, hostgroupname, data):
        ret = self.call_url(
            path="v1/objects/hostgroups/" + hostgroupname,
            data=self.module.jsonify(data),
            method="POST"
        )
        return ret

    def diff(self, hostgroupname, data):
        ret = self.call_url(
            path="v1/objects/hostgroups/" + hostgroupname,
            method="GET"
        )
        changed = False
        ic_data = ret['data']['results'][0]
        for key in data['attrs']:
            if key not in ic_data['attrs'].keys():
                changed = True
            elif data['attrs'][key] != ic_data['attrs'][key]:
                changed = True
        return changed


# ===========================================
# Module execution.
#
def main():
    # use the predefined argument spec for url
    argument_spec = url_argument_spec()
    # remove unnecessary argument 'force'
    del argument_spec['force']
    # add our own arguments
    argument_spec.update(
        state=dict(default="present", choices=["absent", "present"]),
        hostgroupname=dict(required=True, aliases=['hostgroup']),
        zone=dict(default=None),
        template=dict(default=None),
        #active=dict(default=None),
        groups=dict(default=None),
        action_url=dict(default=None),
        #ha_mode=dict(default=None),
        notes=dict(default=None),
        original_attributes=dict(default=None),
        package=dict(default="_etc"),
        paused=dict(default=None),
#        ip=dict(required=True),
        variables=dict(type='dict', default=None),
    )

    # Define the main module
    module = AnsibleModule(
        argument_spec=argument_spec,
        supports_check_mode=True
    )

    hostgroupname = module.params["hostgroupname"]
    zone = module.params["zone"]
    action_url = module.params["action_url"]
    template = []
    #template.append(hostgroupname)
    #if module.params["template"]:
    #    template.append(module.params["template"])
    #active = module.params["active"]
    groups = module.params["groups"]
    #ha_mode = module.params["ha_mode"]
    notes = module.params["notes"]
    original_attributes = module.params["original_attributes"]
    package = module.params["package"]
    paused = module.params["paused"]
    #if not display_name:
    #    display_name = hostgroupname
    variables = module.params["variables"]
    state = module.params["state"]

    try:
        icinga = icinga2_api(module=module)
        icinga.check_connection()
    except Exception as e:
        module.fail_json(msg="unable to connect to Icinga. Exception message: %s" % (e))

    data = {
        'attrs': {
            #'hostgroupname': hostgroupname,
            'zone': zone,
            'action_url': action_url,
            #'active': active,
            'groups': groups,
            #'ha_mode': ha_mode,
            'notes': notes,
            #'original_attributes': original_attributes,
            'package': package,
            #'paused': paused,
            'vars': {
                'made_by': "ansible",
            },
        }
    }

    if variables:
        data['attrs']['vars'].update(variables)

    changed = False
    if icinga.exists(hostgroupname):
        if state == "absent":
            if module.check_mode:
                module.exit_json(changed=True, name=hostgroupname, data=data)
            else:
                try:
                    ret = icinga.delete(hostgroupname)
                    if ret['code'] == 200:
                        changed = True
                    else:
                        module.fail_json(msg="bad return code deleting hostgroup: %s" % (ret['data']))
                except Exception as e:
                    module.fail_json(msg="exception deleting hostgroup: " + str(e))

        elif icinga.diff(hostgroupname, data):
            if module.check_mode:
                module.exit_json(changed=False, name=hostgroupname, data=data)

            # Template attribute is not allowed in modification
#            del data['attrs']['templates']

            ret = icinga.modify(hostgroupname, data)

            if ret['code'] == 200:
                changed = True
            else:
                module.fail_json(msg="bad return code modifying hostgroup: %s" % (ret['data']))

    else:
        if state == "present":
            if module.check_mode:
                changed = True
            else:
                try:
                    ret = icinga.create(hostgroupname=hostgroupname, data=data)
                    if ret['code'] == 200:
                        changed = True
                    else:
                        #message = 
                        module.fail_json(msg="bad return code creating hostgroup: %s" % (ret['data']))
                except Exception as e:
                    module.fail_json(msg="exception creating hostgroup: " + str(e))

    module.exit_json(changed=changed, name=hostgroupname, data=data)


# import module snippets
if __name__ == '__main__':
    main()
