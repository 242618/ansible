#!/usr/bin/python
# -*- coding: utf-8 -*-

# This module is proudly sponsored by CGI (www.cgi.com) and
# KPN (www.kpn.com).
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function
__metaclass__ = type


ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['preview'],
                    'supported_by': 'community'}

DOCUMENTATION = '''
---
module: icinga2_service
short_description: Manage a service for a host in Icinga2
description:
   - "Add or remove a service for a host to Icinga2 through the API."
   - "See U(https://www.icinga.com/docs/icinga2/latest/doc/12-icinga2-api/)"
version_added: "2.5"
author: from icinga2_host.py by "Jurgen Brand (@t794104)" updated for services "Institute for Computer Science - Masaryk University, Brno, Czech Rep. (Tomas Stribula)"
options:
  url:
    description:
      - HTTP, HTTPS, or FTP URL in the form (http|https|ftp)://[user[:pass]]@host.domain[:port]/path
    required: true
  use_proxy:
    description:
      - If C(no), it will not use a proxy, even if one is defined in
        an environment variable on the target hosts.
    type: bool
    default: 'yes'
  validate_certs:
    description:
      - If C(no), SSL certificates will not be validated. This should only be used
        on personally controlled sites using self-signed certificates.
    type: bool
    default: 'yes'
  url_username:
    description:
      - The username for use in HTTP basic authentication.
      - This parameter can be used without C(url_password) for sites that allow empty passwords.
  url_password:
    description:
        - The password for use in HTTP basic authentication.
        - If the C(url_username) parameter is not specified, the C(url_password) parameter will not be used.
  force_basic_auth:
    description:
      - httplib2, the library used by the uri module only sends authentication information when a webservice
        responds to an initial request with a 401 status. Since some basic auth services do not properly
        send a 401, logins will fail. This option forces the sending of the Basic authentication header
        upon initial request.
    type: bool
    default: 'no'
  client_cert:
    description:
      - PEM formatted certificate chain file to be used for SSL client
        authentication. This file can also include the key as well, and if
        the key is included, C(client_key) is not required.
  client_key:
    description:
      - PEM formatted file that contains your private key to be used for SSL
        client authentication. If C(client_cert) contains both the certificate
        and key, this option is not required.
  state:
    description:
      - Apply feature state.
    choices: [ "present", "absent" ]
    default: present
  name:
    description:
      - Name used to create / delete the service on the host: "hostname"!"servicename". This does not need to be the FQDN, but does needs to be unique.
    required: true
  zone:
    description:
      - The zone from where this host should be polled.
  template:
    description:
      - The template used to define the service.
      - Template cannot be modified after object creation.
  check_command:
    description:
      - The command used to call the service.
    required: true
  display_name:
    description:
      - The name used to display the service.
    default: if none is give it is the value of the <name> parameter
  variables:
    description:
      - List of variables.
'''

EXAMPLES = '''
- name: Add service to host in Icinga2
  icinga2_service:
    url: "https://icinga2.example.com"
    url_username: "ansible"
    url_password: "a_secret"
    state: present
    host_name: "host.example.com"
    service_name: "ping4"
'''

RETURN = '''
name:
    description: "host_name!service_name" - the name of host and service which is created, modified or deleted
    type: string
    returned: always
data:
    description: The data structure used for create, modify or delete of the host
    type: dict
    returned: always
'''

import json
import os

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import fetch_url, url_argument_spec


# ===========================================
# Icinga2 API class
#
class icinga2_api:
    module = None

    def __init__(self, module):
        self.module = module

    def call_url(self, path, data='', method='GET'):
        headers = {
            'Accept': 'application/json',
            'X-HTTP-Method-Override': method,
        }
        url = self.module.params.get("url") + "/" + path
        rsp, info = fetch_url(module=self.module, url=url, data=data, headers=headers, method=method, use_proxy=self.module.params['use_proxy'])
        body = ''
        if rsp:
            body = json.loads(rsp.read())
        if info['status'] >= 400:
            body = info['body']
        return {'code': info['status'], 'data': body}

    def check_connection(self):
        ret = self.call_url('v1/status')
        if ret['code'] == 200:
            return True
        return False

    def exists(self, name):
        data = {
            "filter": "match(\"" + name+ "\", service.__name)",
        }
        ret = self.call_url(
            path="v1/objects/services",
            data=self.module.jsonify(data)
        )
        if ret['code'] == 200:
            if len(ret['data']['results']) == 1:
                return True
        return False

    def create(self, name, data):
        ret = self.call_url(
            path="v1/objects/services/" + name,
            data=self.module.jsonify(data),
            method="PUT"
        )
        return ret

    def delete(self, name):
        data = {"cascade": 1}
        ret = self.call_url(
            path="v1/objects/services/" + name,
            data=self.module.jsonify(data),
            method="DELETE"
        )
        return ret

    def modify(self, name, data):
        ret = self.call_url(
            path="v1/objects/services/" + name,
            data=self.module.jsonify(data),
            method="POST"
        )
        return ret

    def diff(self, name, data):
        ret = self.call_url(
            path="v1/objects/services/" + name,
            method="GET"
        )
        changed = False
        ic_data = ret['data']['results'][0]
        for key in data['attrs']:
            if key not in ic_data['attrs'].keys():
                changed = True
            elif data['attrs'][key] != ic_data['attrs'][key]:
                changed = True
        return changed


# ===========================================
# Module execution.
#
def main():
    # use the predefined argument spec for url
    argument_spec = url_argument_spec()
    # remove unnecessary argument 'force'
    del argument_spec['force']
    # add our own arguments
    argument_spec.update(
        state=dict(default="present", choices=["absent", "present"]),
        host_name=dict(required=True, aliases=['host']),
        service_name=dict(required=True, aliases=['service']),
        zone=dict(),
        template=dict(default=None),
        group=dict(default=None),
        check_command=dict(default="passive"),
        enable_active_checks=dict(default="0"),
        max_check_attempts=dict(default=3),
        check_interval=dict(default="5m"),
        display_name=dict(default=None),
        retry_interval=dict(default="1"),
        variables=dict(type='dict', default=None),
    )

    # Define the main module
    module = AnsibleModule(
        argument_spec=argument_spec,
        supports_check_mode=True
    )

    state = module.params["state"]
    host_name = module.params["host_name"] 
    service_name = module.params["service_name"]
    name = host_name + "!" + service_name
    check_interval = module.params["check_interval"]
    zone = module.params["zone"]
    group = []
    if module.params["group"]:
        group.append(module.params["group"])
    template = []
    if module.params["template"]:
        template.append(module.params["template"])
    check_command = module.params["check_command"]
    enable_active_checks = module.params["enable_active_checks"]
    max_check_attempts = module.params["max_check_attempts"]
    retry_interval = module.params["retry_interval"]
    display_name = module.params["display_name"]
    variables = module.params["variables"]

    try:
        icinga = icinga2_api(module=module)
        icinga.check_connection()
    except Exception as e:
        module.fail_json(msg="unable to connect to Icinga. Exception message: %s" % (e))

    data = {
        'attrs': {
            'check_command': check_command,
            'check_interval': check_interval,
            'display_name': display_name,
            'enable_active_checks': enable_active_checks,
            'max_check_attempts': max_check_attempts,
            'retry_interval': retry_interval,
            'templates': template,
            'groups': group,
            'vars': {
                'made_by': "ansible",
            },
        }
    }

    if variables:
        data['attrs']['vars'].update(variables)

    changed = False
    if icinga.exists(name):
        if state == "absent":
            if module.check_mode:
                module.exit_json(changed=True, name=name, data=data)
            else:
                try:
                    ret = icinga.delete(name)
                    if ret['code'] == 200:
                        changed = True
                    else:
                        module.fail_json(msg="bad return code deleting service: %s" % (ret['data']))
                except Exception as e:
                    module.fail_json(msg="exception deleting service: " + str(e))

        elif icinga.diff(name, data):
            if module.check_mode:
                module.exit_json(changed=False, name=name, data=data)

            # Template attribute is not allowed in modification
            del data['attrs']['templates']

            ret = icinga.modify(name, data)

            if ret['code'] == 200:
                changed = True
            else:
                module.fail_json(msg="bad return code modifying service: %s" % (ret['data']))

    else:
        if state == "present":
            if module.check_mode:
                changed = True
            else:
                try:
                    ret = icinga.create(name=name, data=data)
                    if ret['code'] == 200:
                        changed = True
                    else:
                        #message = 
                        module.fail_json(msg="bad return code creating service: %s" % (ret['data']))
                except Exception as e:
                    module.fail_json(msg="exception creating service: " + str(e))

    module.exit_json(changed=changed, name=name, data=data)


# import module snippets
if __name__ == '__main__':
    main()
